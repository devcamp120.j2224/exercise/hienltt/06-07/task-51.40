package com.devcamp.j02_javabasic.s40;

public class WrapperExample {
    
    public static void autoBoxing(){
        byte bte = 11;
        short sh = 21;
        int it = 31;
        long lng = 41;
        float fat = 51.0F;
        double dbl = 61.0D;
        char ch = 'b';
        boolean bool = false;
        /***
         * Autoboxing là cơ chế tự động chuyển đổi dữ liệu nguyên thuỷ sang object
         * của wrapper class tương ứng
         */
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;

        System.out.println("---Printing object values ---");
        System.out.println("Byte object: " + byteobj);
        System.out.println("Short object: " + shortobj);
        System.out.println("Integer object: " + intobj);
        System.out.println("Long object: " + longobj);
        System.out.println("Float object: " + floatobj);
        System.out.println("Double object: " + doubleobj);
        System.out.println("Character object: " + charobj);
        System.out.println("Boolean object: " + boolobj);
    }

    public static void unBoxing(){
        byte bte = 12;
        short sh = 22;
        int it = 32;
        long lng = 42;
        float fat = 52.0F;
        double dbl = 62.0D;
        char ch = 'c';
        boolean bool = true;
        //Autoboxing: Converting primitives into object
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;
        /***
         * Unboxing: Converting object to primitives
         * Unboxing là cơ chế tự động chuyển đổi các object của wrapper
         * class sang kiểu dữ liệu nguyển thuỷ tương ứng
         */
        byte bytevalue = byteobj;
        short shortvalue = shortobj;
        int intvalue = intobj;
        long longvalue = longobj;
        float floatvalue = floatobj;
        double doublevalue = doubleobj;
        char charvalue = charobj;
        boolean boolvalue = boolobj;
        System.out.println("---Printing primitives values ---");
        System.out.println("byte value: " + bytevalue);
        System.out.println("short value: " + shortvalue);
        System.out.println("int value: " + intvalue);
        System.out.println("long value: " + longvalue);
        System.out.println("float value: " + floatvalue);
        System.out.println("double value: " + doublevalue);
        System.out.println("char value: " + charvalue);
        System.out.println("boolean value: " + boolvalue);
    }

    public static void main(String[] asgr){
        WrapperExample.autoBoxing();
        WrapperExample.unBoxing();
    }
}
